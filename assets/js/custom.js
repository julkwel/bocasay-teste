/**
 * scroll button on top to section2
 */
$(document).ready(function ($) {
    $("a").on('click', function (event) {
        if (this.hash === "") {
            return false;
        }

        var hash = this.hash;
        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800, function () {
            window.location.hash = hash;
        });
    });


    $(".nav-item").on('click', function (event) {
        if (this.hash === "") {
            return false;
        }

        $('.nav-item').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');

        event.preventDefault();
        var hash = this.hash;

        $('html, body').animate({
            scrollTop: $(hash).offset().top
        }, 800, function () {
            window.location.hash = hash;
        });
    });
});
